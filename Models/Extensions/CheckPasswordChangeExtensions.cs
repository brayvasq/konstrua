using Microsoft.AspNetCore.Builder;
using Konstrua.Models.Middlewares;

namespace Konstrua.Models.Extensions
{
    public static class CheckPasswordChangeExtensions
    {
        public static IApplicationBuilder UseCheckPasswordChange(
          this IApplicationBuilder builder
        )
        {
          return builder.UseMiddleware<CheckPasswordChange>();
        }
    }
}
