using System;
using Microsoft.AspNetCore.Http;
using System.Globalization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Konstrua.Models.Middlewares
{
    public class CheckPasswordChange
    {
        private readonly RequestDelegate _next;
        public CheckPasswordChange(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context, UserManager<ApplicationUser> userManager)
        {
            var userName = context.User.Identity.Name;
            if (userName != null && userName != "")
            {
                var user = await userManager.FindByEmailAsync(userName);

                if(!user.PasswordChanged && context.Request.Path.Value != "/Identity/Account/Manage/ChangePassword"){
                  context.Response.Redirect(context.Request.PathBase + "/Identity/Account/Manage/ChangePassword");
                }
            }

            await _next(context);
        }
    }
}
