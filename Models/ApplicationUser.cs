using Microsoft.AspNetCore.Identity;

namespace Konstrua.Models
{
    public class ApplicationUser : IdentityUser
    {
        public bool PasswordChanged { get; set; }
    }
}