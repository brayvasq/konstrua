using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Konstrua.Models;
namespace Konstrua.Data.Seeds
{
    public static class UserSeed
    {
        public static async Task InilializeAsync(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetRequiredService<ApplicationDbContext>();
            context.Database.EnsureCreated();

            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();

            await createUserWithRole(userManager, roleManager, "admin@admin.com", "Admin@123", "Admin", true);
            await createUserWithRole(userManager, roleManager, "vendor@vendor.com", "Vendor@123", "Vendor", true);
        }

        public static async Task createUserWithRole(
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager,
            string userEmail,
            string userPassword,
            string roleName,
            bool emailConfirmed
        )
        {

            var userRole = await roleManager.FindByNameAsync(roleName);

            if (!(userRole == null))
            {
                var user = await userManager.FindByEmailAsync(userEmail);
                if (user == null)
                {
                    user = new ApplicationUser()
                    {
                        UserName = userEmail,
                        Email = userEmail,
                        EmailConfirmed = emailConfirmed
                    };

                    var result = await userManager.CreateAsync(user, userPassword);

                    if (result.Succeeded)
                    {
                        result = await userManager.AddToRoleAsync(user, roleName);
                    }
                    else
                    {
                        Console.WriteLine("Error creating user");
                        Console.WriteLine(result.ToString());
                    }
                }
            }
        }
    }
}