# Konstrua Notes
**Creating project**
```bash
dotnet new mvc -au Individual -o Konstrua
```
- `mvc` uses the mvc template.
- `-au` - `--auth` The type of autentication. `Individual` - Individual authentication.
- `-o` Create the project in the folder `Konstrua`.

**Open the project**
```bash
code -r Konstrua
# Or 
cd Konstrua
code .
```

**Run project**
```bash
dotnet watch run
```

**Install packages**
Code generation
```bash
# Entity Framework tools for .NET CLI
dotnet tool install --global dotnet-ef
# Scaffolding dotnet codegeneration
dotnet tool install -g dotnet-aspnet-codegenerator

# To search packages
dotnet tool install --global dotnet-search

# To run project in watch mode
dotnet tool install --global dotnet-watch
```

**Add packages**
```bash
# SQLite support
dotnet add package Microsoft.EntityFrameworkCore.SQLite
# SQLServer supoort
dotnet add package Microsoft.EntityFrameworkCore.SqlServer
# Middleware to detect and diagnose errors with Entity Framework Core migrations
dotnet add package Microsoft.AspNetCore.Diagnostics.EntityFrameworkCore
# Some Identity packages
dotnet add package Microsoft.AspNetCore.Identity.EntityFrameworkCore
dotnet add package Microsoft.AspNetCore.Identity.UI
# Packages needed for scaffolding
dotnet add package Microsoft.VisualStudio.Web.CodeGeneration.Design
dotnet add package Microsoft.EntityFrameworkCore.Design
# Logging
dotnet add package Microsoft.Extensions.Logging.Debug
# Tools
dotnet add package Microsoft.EntityFrameworkCore.Tools
```

**Generate Views**
```bash
# List files
dotnet aspnet-codegenerator identity --listFiles

# Generate specific files
dotnet aspnet-codegenerator identity --useSqLite -dc Konstrua.Data.ApplicationDbContext --files "Account.Register;Account.Login;Account.Logout"
```

**Migrations**
```bash
# Add migration
dotnet ef migrations add AddUserPasswordChanged

# Remove migration
dotnet ef migrations remove --force

# Update database schema
dotnet ef database update
```
- https://docs.microsoft.com/en-us/aspnet/core/security/authentication/customize-identity-model?view=aspnetcore-5.0
- https://docs.microsoft.com/en-us/ef/core/managing-schemas/migrations/?tabs=dotnet-core-cli

**Middlewares**
- https://docs.microsoft.com/en-us/aspnet/core/fundamentals/middleware/?view=aspnetcore-5.0

**References**
- https://www.youtube.com/watch?v=CzRM-hOe35o
- https://dev.to/moe23/step-by-step-guide-on-how-to-add-authentication-to-asp-net-core-mvc-and-how-to-customise-it-14oc
- https://medium.com/@carlostixilema/autenticaci%C3%B3n-y-autorizaci%C3%B3n-por-roles-y-pol%C3%ADticas-en-asp-net-core-15c04015dd49
- https://docs.microsoft.com/es-es/aspnet/core/security/authentication/scaffold-identity?view=aspnetcore-3.1&tabs=netcore-cli
- https://docs.microsoft.com/en-us/aspnet/core/security/authorization/secure-data?view=aspnetcore-5.0
- https://romansimuta.com/posts/initial-adding-admin-roles-and-users-to-the-database-for-role-based-authorization-in-the-asp-net-core-mvc-applications/
- https://www.youtube.com/watch?v=yydVHt2OKHk
- https://www.red-gate.com/simple-talk/dotnet/c-programming/policy-based-authorization-in-asp-net-core-a-deep-dive/
- https://codeburst.io/implement-recaptcha-in-asp-net-core-and-razor-pages-eed8ae720933
- https://github.com/tanveery/recaptcha-net
- https://blog.davem.dev/2019/03/integrating-google-recaptcha-v2-with.html
