using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Konstrua.Data.Seeds;
using Konstrua.Models;

namespace Konstrua.Controllers
{
    [Authorize(Policy = "Write")]
    public class VendorController : Controller
    {
        UserManager<ApplicationUser> userManager;
        RoleManager<IdentityRole> roleManager;

        public VendorController(
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager
        )
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
        }

        public IActionResult Index()
        {
            var users = userManager.Users.ToList();
            return View(users);
        }

        public IActionResult Create()
        {
            return View(new ApplicationUser());
        }

        [HttpPost]
        public async Task<IActionResult> Create(ApplicationUser user)
        {
            await UserSeed.createUserWithRole(
                userManager, roleManager, user.Email, "Vendor@123", "Vendor", true
            );
            return RedirectToAction("Index");
        }
    }
}
